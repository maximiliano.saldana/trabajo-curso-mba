# Trabajo Curso MBA

Trabajo del curso de Modelos Basados en Agentes dictado por Daniel Ciganda, FCEA, UdelaR

## Pauta

Realizar una revisión bibliográfica de MBAs que simulen mercados, estudiar el código en el caso que se encuentre disponible y describir su funcionamiento. En el caso que sea posible correrlo, manipular el código y probar su funcionamiento.

Fecha de entrega: 30/06/2022